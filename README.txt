Google Analytics Custom Variables
by Ben Buckman @ NewLeafDigital.com

********
Note: As of version 6.x-3.1, most of this functionality is now built into the Google Analytics module. Use this supplementary module only if you're using an older version of GA, or if you need the statistics support.
********

This module allows arbitrary/custom variables to be tracked with Google Analytics.
(These variables can then be queried using the Google Analytics API.)

Related modules:
  - Google Analytics API - http://drupal.org/project/google_analytics_api - (for reading the tracked data)
  - Google Analytics Counter - http://drupal.org/project/google_analytics_counter - tracks page stats using the URL/alias as a proxy (not nid directly as this module does)
  - Google Analytics Importer - http://drupal.org/project/ga_importer - similar to Counter but no code released yet

Documentation on custom variables: http://code.google.com/intl/en/apis/analytics/docs/tracking/gaTrackingCustomVariables.html

* How to utilize for your own custom variables:
  This module provides a hook, hook_ga_customvars_define, to define the variable keys. 
  There are 5 slots available, nid is set by default to the 1st slot.
  Then use ga_customvars_set() to set the values. (Similar to the way tokens are defined.)


* * * * * * * * * * * * * * * * * * * 
The submodule ga_customvars_lookup loads the 'nid' custom variable created by default with ga_customvars
and populates the node_counter table as if it were from core node stats. 
See more details in the comments at the top of ga_customvars_lookup.module.

* * * * * * * * * * * * * * * * * * * 

* Implementation notes:
Can use 2 approaches:
  1. Use existing googleanalytics_codesnippet_before variable output in googleanalytics_footer()
    - (append to $GLOBALS['conf']['googleanalytics_codesnippet_before])
    - pros: existing mechanism
    - cons: messy use of variables
  2. Add additional inline JS to footer
    - pros: cleaner (?)
    - cons: need to re-check if page has analytics
  -- going w/ #1 for now

  Variables to set (5 available)
    1. nid for node pages
    2. node type for node pages
    3-5. [custom, set w/ hook]

 ** Sync/async trackers **
  - GA module v3 uses async (_gaq.push) tracker, previous versions use sync pageTracker
  -- need to handle both! but only aware which in JS itself... so both are put w/ JS-side check

