<?php
/**
 * Google Analytics Custom Vars - Views Integration
 * need to spoof core statistics module's hooks, when statistics is off
 */

/**
 * should these hooks be enabled?
 */
function _ga_customvars_views_hooks() {
  // module off, but table exists
  if (module_exists('views') && !module_exists('statistics') && db_table_exists('node_counter')) {
    return TRUE;
  }
  return FALSE;
}


/**
 * Implementation of hook_views_data()
 */
function ga_customvars_lookup_views_data() {
  if (_ga_customvars_views_hooks()) {
    require_once drupal_get_path('module', 'views') . '/modules/statistics.views.inc';
    return statistics_views_data();
  }
}


/**
 * Implementation of hook_views_handlers() to register all of the basic handlers
 * views uses.
 */
function ga_customvars_lookup_views_handlers() {
  if (_ga_customvars_views_hooks()) {
    require_once drupal_get_path('module', 'views') . '/modules/statistics.views.inc';
    return statistics_views_handlers();
  }  
}

